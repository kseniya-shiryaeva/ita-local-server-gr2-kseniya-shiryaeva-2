<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 21.06.18
 * Time: 10:42
 */

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class uLoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';

    /**
     * @Route("/register-case", name="app-register-case")
     */
    public function registerCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = new User();

        $email = $userData['email'] ?? '';
        $user->setEmail($email);


        $form = $this->createForm(
            'App\Form\ULoginRegisterType', $user, [
                'action' => $this
                    ->get('router')
                    ->generate('app-register-case2')
            ]
        );
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя
        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm(
            'App\Form\ULoginRegisterType',
            $user
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setPassword("social------------------------".time());
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("auth", array(
                        'user' => $this->getUser()
                    ));
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/login-case", name="app-login-case")
     *
     * @param UserHandler $userHandler
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginCaseAction(
        UserHandler $userHandler,
        UserRepository $userRepository
    )
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = null;

        if($userData['network'] == 'facebook'){
            $user = $userRepository->getByFacebook(
                $userData['uid']
            );
        } elseif ($userData['network'] == 'vkontakte'){
            $user = $userRepository->getByVkontacte(
                $userData['uid']
            );
        } elseif($userData['network'] == 'google') {
            $user = $userRepository->getByGoogle(
                $userData['uid']
            );
        }

        if ($user) {
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('start_page');
        }

        return $this->redirectToRoute('auth');
    }

    /**
     * @Route("/set-join-case", name="app-set-join-case")
     *
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setJoinCaseAction(
        ObjectManager $manager
    )
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = $this->getUser();

        if($userData['network'] == 'facebook'){
            $user->setFaceBookId(
                $userData['uid']
            );
        } elseif ($userData['network'] == 'vkontakte'){
            $user->setVkId(
                $userData['uid']
            );
        } elseif($userData['network'] == 'google') {
            $user->setGoogleId(
                $userData['uid']
            );
        }

        if ($user) {
            $manager->persist($user);
            $manager->flush();
        }

        return $this->redirectToRoute('client_cabinet');
    }
}