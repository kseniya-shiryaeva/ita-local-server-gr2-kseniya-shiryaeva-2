<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{

    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword()
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("ping");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/auth", name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                $data['password'],
                $data['email']
            );

            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('ping');
            }

            try {
                if ($apiContext->checkClientCredentials(
                    $data['password'],
                    $data['email']
                )) {
                    $centralData = $apiContext->getClientByEmail($data['email']);

                    $user = $userHandler->createNewUser(
                        $centralData,
                        false
                    );
                    $manager->persist($user);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('ping');
                } else {
                    $error = 'Ты не тот, за кого себя выдаешь';
                }
            } catch (ApiException $e) {
                $error = 'Что-то где-то пошло нетак';
            }
        }

        return $this->render(
            '/login.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function indexAction(ApiContext $context)
    {
        return $this->render('index.html.twig', []);

    }

    /**
     * @Route("/cabinet", name="client_cabinet")
     */
    public function showAction()
    {

        $user = $this->getUser();

        $providers = [];

        if($user->getFaceBookId() == null){
            $providers[] = 'facebook';
        }

        if($user->getVkId() == null){
            $providers[] = 'vkontakte';
        }

        if($user->getGoogleId() == null){
            $providers[] = 'google';
        }

        if(count($providers) > 0){
            $providers = implode(',', $providers);
        }

        return $this->render('cabinet.html.twig', [
            'user' => $this->getUser(),
            'providers' => $providers
        ]);

    }

    /**
     * @Route("/ping", name="ping")
     *
     * @param ApiContext $context
     * @return Response
     */
    public function pingAction(ApiContext $context)
    {
        try {
            $result = $context->makePing();
            return new Response(var_export($result, 1));
        } catch (ApiException $e) {
            return new Response($e->getMessage());
        }
    }

    /**
     * @Route("/qwe", name="qwe")
     *
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function qweAction(ApiContext $apiContext)
    {
        $result = $apiContext->clientExists('passport 111', 'kkk@kkk.ru');

        return new Response(var_export($result, 1));
    }

    /**
     * @Route("/start_page", name="start_page")
     *
     * @param Request $request
     * @return Response
     */
    public function startWorkAction(Request $request)
    {
        return $this->render('start_page.html.twig', [
            'user' => $this->getUser()
        ]);
    }
}
