<?php
namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewUser([
            'email' => 'kkk@kkk.ru',
            'passport' => 'passport 111',
            'password' => 'pass_1234',
        ]);

        $manager->persist($user);
        $manager->flush();
    }
}
